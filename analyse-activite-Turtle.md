
# Objectifs
Apprendre à enchainer une série d'instructions pour obtenir un résultat
Introduction au concept de boucles

# Pré-requis à cette activité
Savoir manipuler des angles

# Durée de l'activité
1h

# Exercices cibles
...

# Description du déroulement de l'activité
TP devant un écran
Déconnecté si pas de PC

# Anticipation des difficultés des élèves


# Gestion de l'hétérogénéïté
Plusieurs niveaux d'exercices existent